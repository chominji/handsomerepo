package com.thehandsome.app.service;

import com.thehandsome.app.dto.MemberDTO;

public interface MemberService {
	MemberDTO getMemberInfo(String id);
}
