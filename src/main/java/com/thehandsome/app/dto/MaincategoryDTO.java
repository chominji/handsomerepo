package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class MaincategoryDTO {
	private int id;
	private String name;
	private String ename;
}
