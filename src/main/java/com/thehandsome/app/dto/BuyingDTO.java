package com.thehandsome.app.dto;

import java.util.Date;

public class BuyingDTO {
	int userId;
	int productId;
	Date orderdate;
	String pick;
	int departmentId;
}
