package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class ProductcolorDTO {
	private String product_id;
	private String color;
	private String imagePath;
}
