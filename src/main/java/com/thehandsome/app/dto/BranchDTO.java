package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class BranchDTO {
	private int id;
	private int department_id;
	private int brand_id;
}

