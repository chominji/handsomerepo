package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class ProductsizeDTO {
	private String product_id;
	private String sizelabel;
}
